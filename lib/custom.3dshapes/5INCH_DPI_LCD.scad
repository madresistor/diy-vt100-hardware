color("white") {
    cube([120.7,  75.9, 2.9], true);
}

color("PaleGoldenrod", 0.7) {
    translate([0,2.07,0]) {
        cube([110.7, 67.5, 2.91], true);
    }
}

color("black", 0.7) {
    translate([0,2.3,0]) {
        cube([108, 64.8, 2.92], true);
    }
}